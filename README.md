A Very Long and Confusing Way to Bnay an Even Number, dot Java.
===============================================================
A very simple, concise Java class to tell you if a given number is odd or even.

Copyright © 2011, 2013 Scott Zeid.  Released under the X11 License.  
<http://code.s.zeid.me/a-very-long-and-confusing-way-to-bnay-an-even-number>

 

This is a *very* simple, concise Java class that tells you whether a given number
is even or not ("bnay an even number").  In fact, it's so simple a caveman can
figure out how it works!

FAQ
---

1.  **What's wrong with RISD?**  
    Nothing, really.  In fact, they have this really cool [Bring Your Own
    Device][byod] policy that finally lets kids use their phones and laptops
    in class!  (Too bad they started it two years after I graduated....)
    
2.  **Well, did they really enforce the old policy?**  
    Depends on the teacher.  Actually, I coded the bulk of [AppBackup 2.0][appbackup]
    in my Precal class and the teacher actually thought it was cool.  I was
    even compiling in choir one day when I went up to the girl I liked and
    asked her to sign my yearbook.

3.  **Um... so is that who Emily is?**  
    That depends.  Who are you, and who do you work for?

4.  **Actually, I'm a full-time college student at—**  
    Go away, nothing to see here—oh wait, did you say UTD?  Sorry, for a
    moment there, I thought you were someone else.

5.  **So, why exactly did you write this?**  
    I was bored, and I thought it would be funny.

6.  **How does it work?**  
    Beats me.  Figure it out and [you get a cookie][cookie]!

7.  **Can I use it in my project?**  
    I don't know why you'd want to, but it's released under the X11 License,
    so go right ahead.

8.  **Can I use it for work?**  
    SURE!!!  I *LOVE* seeing huge, too-big-to-fail businesses use shitty-ass
    code!  Be my guest!  :D

To Do
-----
 *  Make it even longer and more confusing
 *  Add some [factory][factory] and [factory factory][factory-factory] classes
 *  Possibly more levels of meta-factories
 *  More Emily references (\*sigh\* yes, Sharon, I'm still over her, it's
    just for lulz)
 *  Write a blurb about who Bnay is
 *  `public static boolean Emily(String num)` could be broken up into about
    twenty smaller methods
 *  Cookies




[byod]:             http://www.risd.org/group/departments/instructional_technology/InstructionalTechnology_Docs/BYOD-Manual.pdf
[appbackup]:        http://s.zeid.me/projects/appbackup/
[cookie]:           http://i.imgur.com/Bk3hX7z.gif
[factory]:          http://i.qkme.me/35xqyh.jpg
[factory-factory]:  http://discuss.joelonsoftware.com/default.asp?joel.3.219431.12
